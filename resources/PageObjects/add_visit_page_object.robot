*** Settings ***
Documentation
Library                                   SeleniumLibrary
Resource                                  homepage_page_object.robot

*** Variables ***
${DATE_FIELD}                             id=date
${DESCRIPTION_FIELD}                      id=description
${SUBMIT_ADD_VISIT_BUTTON}                Add Visit
${NEW_VISIT_ELEMENT}                      //h2[normalize-space()='New Visit']

*** Keywords ***
Fill New Visit Fields From Given List
    [Arguments]                           &{new_visit}
    Input Text                            ${DATE_FIELD}           &{new_visit}[date]
    Input Text                            ${DESCRIPTION_FIELD}    &{new_visit}[description]

Click Submit Visit Button
    Click Button                          ${SUBMIT_ADD_VISIT_BUTTON}

Adding Visit Should Have Succeeded
    Page Should Not Contain Element       ${NEW_VISIT_ELEMENT}
