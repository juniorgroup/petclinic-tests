*** Settings ***
Documentation
Library                                   SeleniumLibrary

*** Variables ***
${PETCLINIC_URL}                          http://maven:8080/
${BROWSER}                                chrome
${GRID_URL}                               http://hub:4444/wd/hub
${PETCLINIC_ELEMENT}                      Welcome
${HEADER_HOME_BUTTON_TEXT}                "Home"
${HEADER_FIND_OWNERS_BUTTON_TEXT}         "Find owners"

*** Keywords ***
Open Petclinic Homepage
    [Documentation]
    Open Browser                          ${PETCLINIC_URL}   ${BROWSER}    None    ${GRID_URL}    version:ANY

Click Header Button
    [Arguments]                           ${button_text}
    Click Link                            xpath=.//*[@id="main-navbar"]/ul/li/a[contains(., ${button_text})]

Navigate To Find Owners Page
    Click Header Button                   ${HEADER_FIND_OWNERS_BUTTON_TEXT}

Navigate To Homepage
    Click Header Button                   ${HEADER_HOME_BUTTON_TEXT}




