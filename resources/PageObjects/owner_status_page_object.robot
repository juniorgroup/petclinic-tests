*** Settings ***
Documentation
Library                                   SeleniumLibrary
Resource                                  homepage_page_object.robot

*** Variables ***
${EDIT_OWNER_LINK}                        Edit Owner
${ADD_NEW_PET_LINK}                       Add New Pet
${ADD_NEW_VISIT_LINK}                     Add Visit

*** Keywords ***
Should Have Pet Named
    [Arguments]                           ${pet_name}
    Page Should Contain                   ${pet_name}

Should Have Visit
    [Arguments]                           &{visit}
    Page Should Contain                   &{visit}[date]
    Page Should Contain                   &{visit}[description]

Click Add New Pet Link
    Click Link                            ${ADD_NEW_PET_LINK}

Click Add New Visit Link
    Click Link                            ${ADD_NEW_VISIT_LINK}
