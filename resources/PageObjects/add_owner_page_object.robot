*** Settings ***
Documentation
Library                                   SeleniumLibrary
Resource                                  homepage_page_object.robot

*** Variables ***
${SUBMIT_NEW_OWNER_BUTTON}                Add Owner
${FIRST_NAME_FIELD}                       id=firstName
${LAST_NAME_FIELD}                        id=lastName
${ADDRESS_FIELD}                          id=address
${CITY_FIELD}                             id=city
${TELEPHONE_FIELD}                        id=telephone

*** Keywords ***
Fill New Owner Fields From Given List
    [Arguments]                           &{owner_values}
    Input Text                            ${FIRST_NAME_FIELD}     &{owner_values}[firstname]
    Input Text                            ${LAST_NAME_FIELD}      &{owner_values}[lastname]
    Input Text                            ${ADDRESS_FIELD}        &{owner_values}[address]
    Input Text                            ${CITY_FIELD}           &{owner_values}[city]
    Input Text                            ${TELEPHONE_FIELD}      &{owner_values}[telephone]

Click Submit Owner Button
    Click Button                          ${SUBMIT_NEW_OWNER_BUTTON}
