*** Settings ***
Documentation
Library                                   SeleniumLibrary
Resource                                  homepage_page_object.robot

*** Variables ***
${FIND_OWNERS_BUTTON}                     Find Owner
${ADD_OWNER_LINK}                         Add Owner
${FIND_OWNERS_FIELD}                      id=lastName
${CANNOT_FIND_OWNER_ELEMENT}             "has not been found"

*** Keywords ***
Find Owner
    [Arguments]                           ${owner_to_be_found}
    Input Text                            ${FIND_OWNERS_FIELD}    ${owner_to_be_found}
    Click Find Owners Button

Check If Owner Search Failed
    Page Should Not Contain               ${CANNOT_FIND_OWNER_ELEMENT}

Click Add Owner Link
    Click Link                            ${ADD_OWNER_LINK}

Click Find Owners Button
    Click Button                          ${FIND_OWNERS_BUTTON}
