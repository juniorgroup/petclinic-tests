*** Settings ***
Documentation
Library                                   SeleniumLibrary
Resource                                  homepage_page_object.robot

*** Variables ***
${PET_NAME_FIELD}                         id=name
${PET_BIRTHDAY_FIELD}                     id=birthDate
${PET_TYPE_DROPDOWN}                      id=type
${SUBMIT_NEW_PET_BUTTON}                  Add Pet
${NEW_PET_ELEMENT}                        //h2[normalize-space()='New Pet']

*** Keywords ***
Fill New Pet Fields From Given List
    [Arguments]                           &{pet_values}
    Input Text                            ${PET_NAME_FIELD}          &{pet_values}[pet]
    Input Text                            ${PET_BIRTHDAY_FIELD}      &{pet_values}[birth_date]
    Select From List By Label             ${PET_TYPE_DROPDOWN}       &{pet_values}[type]

Click Submit New Pet Button
    Click Button                          ${SUBMIT_NEW_PET_BUTTON}

Adding Pet Should Have Succeeded
    Page Should Not Contain Element           ${NEW_PET_ELEMENT}

