SETUP_OWNER =  {"firstname":"Pekka", "lastname":"Meikalainen","address": "Jokuosoite 1", "city": "Turku", "telephone":98765432}
SETUP_PETS =   [{"pet":"Musti5", "owner":"Franklin", "birth_date": "1999-12-12", "type":"dog"},
               {"pet":"Musti6", "owner":"Franklin", "birth_date": "1999-12-12", "type":"dog"},
               {"pet":"Musti7", "owner":"Franklin", "birth_date": "1999-12-12", "type":"dog"}]
SETUP_VISITS = [{"date":"2018-12-12" ,"description":"A description","owner":"Franklin","pet":"Musti5"},
               {"date":"2018-12-12" ,"description":"A description","owner":"Franklin","pet":"Musti6"},
               {"date":"2018-12-12" ,"description":"A description","owner":"Franklin","pet":"Musti7"}]
TEST_PET =     {"pet":"Musti", "owner":"Franklin", "birth_date": "1999-12-12", "type":"dog"}
TEST_VISIT =   {"date":"2018-12-12" ,"description":"A description","owner":"Franklin","pet":"Musti"}
