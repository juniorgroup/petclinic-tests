*** Settings ***
Documentation
Library                                        SeleniumLibrary
Resource                                       PageObjects/homepage_page_object.robot
Resource                                       PageObjects/find_owner_page_object.robot
Resource                                       PageObjects/add_owner_page_object.robot
Resource                                       PageObjects/owner_status_page_object.robot
Resource                                       PageObjects/add_new_pet_page_object.robot
Resource                                       PageObjects/add_visit_page_object.robot
Variables                                      variables.py

*** Keywords ****
Add New Owner
    [Arguments]                                &{given_owner}
    Navigate To Find Owners Page
    Click Add Owner Link
    Fill New Owner Fields From Given List      &{given_owner}
    Click Submit Owner Button

Add New Pet To An Owner
    [Arguments]                                &{given_pet}
    Navigate To Find Owners Page
    Find Owner                                 &{given_pet}[owner]
    Click Add New Pet Link
    Fill New Pet Fields From Given List        &{given_pet}
    Click Submit New Pet Button

Add Test Pet To An Owner
    Add New Pet To An Owner                    &{TEST_PET}

Add New Visit To A Pet
    [Arguments]                                &{given_visit}
    Navigate To Find Owners Page
    Find Owner                                 &{given_visit}[owner]
    Click Add New Visit Link
    Fill New Visit Fields From Given List      &{given_visit}
    Click Submit Visit Button

Add Test Visit To A Pet
    Add New Visit To A Pet                     &{TEST_VISIT}

Confirm Pet Can Be Found On The Website
    [Arguments]                                &{pet_to_exist}
    Navigate To Find Owners Page
    Find Owner                                 &{pet_to_exist}[owner]
    Should Have Pet Named                      &{pet_to_exist}[pet]

Confirm Pet Can Be Found In The Database
    [Arguments]                                ${pet_to_exist}
    Check If Exists In Database                select id from pets where name = '${pet_to_exist}'

Confirm Test Pet Can Be Found In The Database
    Confirm Pet Can Be Found In The Database   &{TEST_PET}[pet]

Confirm Test Pet Can Be Found On The Website
    Confirm Pet Can Be Found On The Website    &{TEST_PET}

Confirm Visit Can Be Found On The Website
    [Arguments]                                &{visit}
    Navigate To Find Owners Page
    Find Owner                                 &{visit}[owner]
    Should Have Visit                          &{visit}

Confirm Test Visit Can Be Found On The Website
    Confirm Visit Can Be Found On The Website  &{TEST_VISIT}
