*** Settings ***
Documentation                             Basic www-tests for petclinic project
Library                                   SeleniumLibrary
Library                                   DatabaseLibrary
Library                                   Remote  http://xvfb:8270  WITH NAME  xvfb
Library                                   Remote  http://i3wm:8270  WITH NAME  i3wm
Library                                   Remote  http://ffmpeg:8270  WITH NAME  ffmpeg
Resource                                  ../resources/main_object.robot
Variables                                 ../resources/variables.py
Suite Setup                               Initialize System
Suite Teardown                            Suite Teardown

*** Test Cases ***

Test Adding Pet
    [Tags]                               218f98e3-c991-416b-a3ce-509e76ef4427
    Add Test Pet To An Owner
#    Adding Pet Should Have Succeeded
    Confirm Test Pet Can Be Found On The Website
    Confirm Test Pet Can Be Found In The Database



Test Adding Visit For A Pet
    [Tags]                               197e2499-bae5-4ebf-8e17-0a6883fef40e
    Add Test Visit To A Pet
    Adding Visit Should Have Succeeded
    Confirm Test Visit Can Be Found On The Website

*** Keywords ****

Initialize System
    Connect To Database                   mysql.connector     petclinic     root     petclinic     mysql    3306
    Open Display
    Open Petclinic Homepage
    Add New Owner                          &{SETUP_OWNER}
    : FOR    ${Pet}           IN           @{SETUP_PETS}
    \  Add New Pet To An Owner             &{Pet}
    : FOR    ${Visit}           IN         @{SETUP_VISITS}
    \  Add New Visit To A Pet              &{Visit}

Suite Teardown
    Close Browser
    Close Display

